package com.pha3l.modernlogistics.config;

import com.pha3l.modernlogistics.ModernLogistics;
import com.pha3l.modernlogistics.proxy.CommonProxy;
import net.minecraftforge.common.config.Configuration;
import org.apache.logging.log4j.Level;

public class Config {
    private static final String CATEGORY_GENERAL = "general";
    private static final String CATEGORY_POWER = "power";

    // Public static vars
    public static boolean enableThing = true;

    public static void readConfig() {
        Configuration cfg = CommonProxy.config;
        try {
            cfg.load();
            initGeneralConfig(cfg);
            initPowerConfig(cfg);
        } catch (Exception e) {
            ModernLogistics.logger.log(Level.ERROR, "Could not load configuration file!", e);
        } finally {
            if (cfg.hasChanged())
                cfg.save();
        }
    }

    private static void initGeneralConfig(Configuration cfg) {
        cfg.addCustomCategoryComment(Configuration.CATEGORY_GENERAL, "General Configuration");
        // cfg.getBoolean() will get the value in the config if it is already specified there. If not it will create the value.
        enableThing = cfg.getBoolean("enableThing", Configuration.CATEGORY_GENERAL, enableThing, "set to false if you want to disable the thing" );

    }

    private static void initPowerConfig(Configuration cfg) {

    }

}
