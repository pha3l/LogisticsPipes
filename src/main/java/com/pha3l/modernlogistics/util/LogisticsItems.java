package com.pha3l.modernlogistics.util;

import com.pha3l.modernlogistics.items.FirstItem;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class LogisticsItems {
    @GameRegistry.ObjectHolder("modernlogistics:firstitem")
    public static FirstItem firstItem;

    @SideOnly(Side.CLIENT)
    public static void initModels()
    {}
}
