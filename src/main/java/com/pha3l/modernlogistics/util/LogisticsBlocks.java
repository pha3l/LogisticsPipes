package com.pha3l.modernlogistics.util;

import com.pha3l.modernlogistics.blocks.DataBlock;
import com.pha3l.modernlogistics.blocks.FirstBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class LogisticsBlocks {
    @GameRegistry.ObjectHolder("modernlogistics:firstblock")
    public static FirstBlock firstBlock;

    @GameRegistry.ObjectHolder("modernlogistics:datablock")
    public static DataBlock dataBlock;

    @SideOnly(Side.CLIENT)
    public static void initModels() {
        LogisticsBlocks.dataBlock.initModel();
    }
}
