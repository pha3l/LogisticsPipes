package com.pha3l.modernlogistics.proxy;

import com.pha3l.modernlogistics.ModernLogistics;
import com.pha3l.modernlogistics.blocks.DataBlock;
import com.pha3l.modernlogistics.blocks.FirstBlock;
import com.pha3l.modernlogistics.config.Config;
import com.pha3l.modernlogistics.items.FirstItem;
import com.pha3l.modernlogistics.tileentity.DataTileEntity;
import com.pha3l.modernlogistics.util.LogisticsBlocks;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.io.File;

@Mod.EventBusSubscriber
public class CommonProxy {

    public static Configuration config;

    public void preInit(FMLPreInitializationEvent event) {
        File directory = event.getModConfigurationDirectory();
        config = new Configuration(new File(directory.getPath(), "modernlogistics.cfg"));
        Config.readConfig();
    }

    public void init(FMLInitializationEvent event) {
    }

    public void postInit(FMLPostInitializationEvent event) {
        if (config.hasChanged())
            config.save();
    }

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {
        //Blocks
        event.getRegistry().register(new FirstBlock());
        event.getRegistry().register(new DataBlock());

        //Tile Entities
        GameRegistry.registerTileEntity(DataTileEntity.class, ModernLogistics.MODID + "_datablock");
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        //ItemBlocks
        event.getRegistry().register(new ItemBlock(LogisticsBlocks.firstBlock).setRegistryName(LogisticsBlocks.firstBlock.getRegistryName()));
        event.getRegistry().register(new ItemBlock(LogisticsBlocks.dataBlock).setRegistryName(LogisticsBlocks.dataBlock.getRegistryName()));

        //Items
        event.getRegistry().register(new FirstItem());
    }
}
