package com.pha3l.modernlogistics;

import com.pha3l.modernlogistics.proxy.CommonProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;


@Mod(modid= ModernLogistics.MODID, name= ModernLogistics.MODNAME, version = ModernLogistics.MODVERSION, useMetadata = true)
public class ModernLogistics {

    public static final String MODID = "assets/modernlogistics";
    public static final String MODNAME = "Modern Logistics";
    public static final String MODVERSION = "0.0.1";

    @SidedProxy(clientSide = "com.pha3l.modernlogistics.proxy.ClientProxy", serverSide = "com.pha3l.modernlogistics.proxy.ServerProxy")
    public static CommonProxy proxy;

    public static Logger logger;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        proxy.preInit(event);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }


}
